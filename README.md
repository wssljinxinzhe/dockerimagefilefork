# Docker Image Fileの作成方


- 前提条件
- こちらのサンプルを実行する為には、自分のLocalで、
- 既に、Dockerインストル済みとなります。
- Downloadこちから、参考にしてください。
- https://qiita.com/n-yamanaka/items/ddb18943f5e43ca5ac2e
## １、Dockerとは、何ですか。

- こちらのコンポーネントを理解する為に、まず、Dockerについて、理解してください。

- こちらのDocker公式サイトを参考になるかもしれません。

- https://docs.docker.com/engine/docker-overview/

## ２、自分のプロジェクトをDocker上で動く為に。

- 自分のプロジェクトをDocker上で動く為には、自分のプロジェクトをDockerImageFileにしなくてはなりません。

- DockerImageFileを作成する為に、自分のプロジェクトのルートディレクトリ下に、DockerFileを作成する必要があります。
 

## ３、最初サンプルプロジェクトを作ります

- HelloDockerフォルダを作成して、main.goファイルに下のコードを貼り付けてください。

- こちらのサンプルとしては、非常に簡単なGo言語のプログラムです。

- もし、Goには、興味があるとしたら、こちらのサイト参考になるかもしれない。

- http://gihyo.jp/dev/feature/01/go_4beginners/
####


```go

      package main

      import (
          "net/http"

          "fmt"
      )
    
      func main() {
    
          http.HandleFunc("/hello", hello)
    
          http.ListenAndServe(":8080", nil)
    
      }
    
      func hello(w http.ResponseWriter, r *http.Request) {
    
          fmt.Fprintf(w, "Hello Docker Form Go!")
    
      }
    
```

## ４、次は、DockerImageFile作成するためには、DockerFileを作成します。

- 下のDockerFileに使われていた環境関数が、ただこちらのGoのサンプルプロジェクトを動く為の必要の依存パケッジ、や実行コマンドなどを記述されています。

- 詳しく、DockerFileについて、理解しようとしたら、こちらのサイトを参考してください。

- https://qiita.com/mk0812/items/c77fec14ec3c281a47b4

- HelloDockerのフォルダの下に、DockerFileを作成して、下のコードを貼り付けてください。
## 
          FROM golang

          MAINTAINER Jinxinzhe

          WORKDIR $GOPATH/src/godocker

          ADD . $GOPATH/src/godocker

          RUN go build main.go

          EXPOSE 8080

          ENTRYPOINT ["./main"]

## 5、ターミナルで、下のコマンドで、DockerImageFileを作成できます。
## 

          docker build -t hellodocker .

## ６、ImageFile Check

- 下のコマンドを実行すると、hellodockerイメージファイルを生成されるはずです。
## 

          docker image ls


## 7、ImageFileを実行する
## 

          docker run -d -p 8080:8080 hellodocker

## 8、Go to Check

- Go to [http://localhost:8080/hello](http://localhost:8080/hello)
## 

          Hello Docker Form Go!
##

	
- 表示されするはず


## 9、結論

- ここまで、記述されたのは、あくまでDockerの基本の動き方しかないです。
- 詳しく勉強しようと思う方は、最初の公式サイトで、勉強してください。